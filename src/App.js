import { Fragment } from "react/cjs/react.production.min";
import "./App.css";

function App() {
  return (
    <Fragment>
      <div className="container">
        <div className="pro">
          <div className="pro-text">
            <h1> GOT MARKETING? ADVANCE YOUR BUSINESS INSIGHT.</h1>
            <p>Fill out the form and receive our award winning newsletter.</p>
          </div>
        </div>
        <div className="sign-up">
          <form>
            <div className="name-container">
              <label htmlFor="name">Name</label>
              <input type="text" id="name"></input>
            </div>
            <div className="email-container">
              <label htmlFor="email">Email</label>
              <input type="email" id="email"></input>
            </div>
            <button>Sign me up</button>
          </form>
        </div>
      </div>
    </Fragment>
  );
}

export default App;

// THIS IS CHANNY'S! DO NOT COPY!
